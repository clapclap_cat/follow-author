<?php
define( 'FOLLOW_AUTHOR_DB_TABLE_NAME', 'follow_author' );
/**
 * Plugin Name: Follow Author
 */
class Follow_Author {
	public function init() {
		load_plugin_textdomain( 'follow-author', false, dirname( plugin_basename( __FILE__ ) ) );
		add_action( 'parse_request', array( __CLASS__, 'update_follows' ) );
		add_action(
			'clapclap_theme_author_content_before',
			function () {
				$user_id   = get_current_user_id();
				$author_id = get_the_author_meta( 'ID' );
				echo wp_kses_post( self::get_follow_button( $user_id, $author_id ) );
			}
		);
		add_filter(
			'clapclap_author_list_after',
			function ( $html, $author_id ) {
				$user_id = get_current_user_id();
				return $html . self::get_follow_button( $user_id, $author_id );
			},
			10,
			2
		);
	}

	public static function update_follows(): void {
		global $wpdb;

		$nonce    = empty( $_REQUEST['_wpnonce'] ) ? '' : sanitize_key( $_REQUEST['_wpnonce'] );
		$follow   = '';
		$unfollow = '';
		if ( ! empty( $_GET['follow'] ) && wp_verify_nonce( $nonce, 'follow_nonce' ) ) {
			$follow = intval( $_GET['follow'] );
		}
		if ( ! empty( $_GET['unfollow'] ) && wp_verify_nonce( $nonce, 'unfollow_nonce' ) ) {
			$unfollow = intval( $_GET['unfollow'] );
		}

		if ( ! is_int( $follow ) && ! is_int( $unfollow ) ) {
			return;
		}

		$user_id = get_current_user_id();
		if ( ! $user_id ) {
			wc_add_notice( wc_kses_notice( sprintf( __( 'You need an active subscription to follow creators. %1$sSubscribe now%2$s', 'follow-author' ), '<a href="' . get_permalink( 38 ) . '" class="button">', '</a>' ) ), 'notice' );
			return;
		}

		if ( is_int( $follow ) && ! self::is_user_following_author( $user_id, $follow ) ) {
			if ( ! class_exists( 'ClapClap_Subscriptions' ) || ! ClapClap_Subscriptions::user_is_subscribed( $user_id ) ) {
				wc_add_notice( wc_kses_notice( sprintf( __( 'You need an active subscription to follow creators. %1$sSubscribe now%2$s', 'follow-author' ), '<a href="' . get_permalink( 38 ) . '" class="button">', '</a>' ) ), 'notice' );
				return;
			}

			$author_to_follow = get_user_by( 'id', $follow );
			$wpdb->insert(
				FOLLOW_AUTHOR_DB_TABLE_NAME,
				array(
					'user_id'   => $user_id,
					'author_id' => $follow,
				),
				array(
					'%d',
					'%d',
				)
			);

			/* translators: %s name of the creator. */
			wc_add_notice( wc_kses_notice( sprintf( __( 'You started to follow %s! From now on their contents will appear on your frontpage.', 'follow-author' ), $author_to_follow->display_name ) ), 'success' );
		}

		if ( is_int( $unfollow ) && self::is_user_following_author( $user_id, $unfollow ) ) {
			$author_to_unfollow = get_user_by( 'id', $unfollow );
			$wpdb->delete(
				FOLLOW_AUTHOR_DB_TABLE_NAME,
				array(
					'user_id'   => $user_id,
					'author_id' => $unfollow,
				)
			);

			/* translators: %s name of the creator. */
			wc_add_notice( wc_kses_notice( sprintf( __( 'You stopped following %s. Their contents will no longer appear in your frontpage.', 'follow-author' ), $author_to_unfollow->display_name ) ), 'notice' );
		}
	}

	public static function get_follow_button( int $user_id, int $author_id ): string {
		if ( $user_id === $author_id ) {
			return '<button disabled title="' . esc_attr__( 'You cannot follow yourself.', 'follow-author' ) . '">' . esc_html__( 'Follow', 'follow-author' ) . '</button>';
		}
		$follows     = self::get_authors_followed_by( $user_id );
		$is_followed = self::is_user_following_author( $user_id, $author_id );

		if ( ! $user_id ) {
			return '<div class="follow-button-wrapper"><a href="' . get_permalink( 1015 ) . '" class="follow-button button">' . esc_html__( 'Follow', 'follow-author' ) . '</a></div>';
		}

		if ( $is_followed ) {
			$unfollow_url = '?unfollow=' . esc_attr( $author_id );
			return '<div class="follow-button-wrapper"><a href="' . wp_nonce_url( $unfollow_url, 'unfollow_nonce' ) . '" class="follow-button button">' . esc_html__( 'Unfollow', 'follow-author' ) . '</a></div>';
		}

		$follow_url = '?follow=' . esc_attr( $author_id );
		return '<div class="follow-button-wrapper"><a href="' . wp_nonce_url( $follow_url, 'follow_nonce' ) . '" class="follow-button button">' . esc_html__( 'Follow', 'follow-author' ) . '</a></div>';
	}

	public static function is_user_following_author( int $user_id, int $author_id ): bool {
		global $wpdb;
		$is_followed = $wpdb->get_var( $wpdb->prepare( 'SELECT author_id FROM ' . esc_sql( FOLLOW_AUTHOR_DB_TABLE_NAME ) . ' WHERE user_id = %d AND author_id = %d', $user_id, $author_id ) );
		return (bool) $is_followed;
	}

	public static function get_authors_followed_by( int $user_id ): array {
		global $wpdb;
		$followed = $wpdb->get_col( $wpdb->prepare( 'SELECT author_id FROM ' . esc_sql( FOLLOW_AUTHOR_DB_TABLE_NAME ) . ' WHERE user_id = %d', $user_id ) );
		return $followed;
	}

	public static function get_number_of_followers( int $author_id ): int {
		if ( ! is_int( $author_id ) ) {
			return 0;
		}
		global $wpdb;
		$number_of_followers = $wpdb->get_var( $wpdb->prepare( 'SELECT COUNT(*) as followers_num FROM ' . esc_sql( FOLLOW_AUTHOR_DB_TABLE_NAME ) . ' WHERE author_id = %d', $author_id ) );
		return $number_of_followers;
	}

	public static function get_authors_with_more_followers( int $limit ): array {
		if ( -1 === $limit ) {
			$limit = 100;
		}
		global $wpdb;
		$top_authors = $wpdb->get_col( $wpdb->prepare( 'SELECT author_id, COUNT(user_id) as followers_num FROM ' . esc_sql( FOLLOW_AUTHOR_DB_TABLE_NAME ) . ' GROUP BY author_id ORDER BY followers_num DESC LIMIT %d', $limit ) );
		return $top_authors;
	}
}

$follow_author = new Follow_Author();
add_action( 'init', array( $follow_author, 'init' ) );
